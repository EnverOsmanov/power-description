#[macro_use]
extern crate log;
extern crate pretty_env_logger;

use std::env;
use std::thread::sleep;

use chrono::{Datelike, DateTime, Local, TimeZone, Utc};
use chrono::NaiveDate;
use chrono::Duration;
use chrono::Weekday::*;
use chrono_tz::Europe::Kyiv;
use frankenstein::{Api, ChatId};
use frankenstein::GetChatParams;
use frankenstein::SetChatDescriptionParams;
use frankenstein::TelegramApi;

fn main() {
    pretty_env_logger::init();

    loop {
        let format = "%H:%M %A";

        let local_datetime_now = utc_2_local(Utc::now());

        let (text, next_date) = find_text(local_datetime_now);

        let full_text = format!("{} at:\n{}", text, (next_date).format(format));
        set_description(&full_text);
        info!("{}", full_text);

        let diff = next_date - local_datetime_now;
        sleep(diff.to_std().unwrap());
    }
}

fn utc_2_local(dt_tz: DateTime<Utc>) -> DateTime<Local> {
    let naive = dt_tz.with_timezone(&Kyiv).naive_local();

    Local.from_local_datetime(&naive).unwrap()
}

fn find_text(todate_time: DateTime<Local>) -> (&'static str, DateTime<Local>) {
    let mon = NaiveDate::from_isoywd_opt(todate_time.year(), todate_time.iso_week().week(), Mon).unwrap();

    let mon_time = mon.and_hms_opt(0, 0, 0).unwrap();

    let local_start_week = Local.from_local_datetime(&mon_time).unwrap();
    let local_end_week = local_start_week + Duration::weeks(1);

    fn find_next_text(
        prev_switch_time: DateTime<Local>,
        todate_time: DateTime<Local>,
        local_end_week: DateTime<Local>,
    ) -> (&'static str, DateTime<Local>) {
        let till_down = prev_switch_time + Duration::hours(5);
        let till_up = till_down + Duration::hours(4);


        let local_till_down = till_down;
        let local_till_up = till_up;

        if prev_switch_time <= todate_time && todate_time <= local_till_down {
            ("❌️", local_till_down)
        } else if local_till_down <= todate_time && todate_time <= local_till_up {
            ("⚡", local_till_up)
            /*        } else if local_end_week <= todate_time {
                        ("Shouldn't be here", todate_time)*/
        } else {
            find_next_text(till_up, todate_time, local_end_week)
        }
    }

    let mon_drift = local_start_week + Duration::hours(1);

    if local_start_week <= todate_time && todate_time <= mon_drift {
        ("⚡", mon_drift)
    } else {
        find_next_text(mon_drift, todate_time, local_end_week)
    }
}

pub fn set_description(text: &str) {
    let chat_id: i64 = env::var("CHAT_ID").unwrap().parse().unwrap();

    let telegram_token: String = env::var("TOKEN").unwrap();

    let set_chat_description_params = SetChatDescriptionParams::builder()
        .chat_id(ChatId::from(chat_id))
        .description(text)
        .build();

    let api = Api::new(&telegram_token);

    let get_chat_params = GetChatParams::builder()
        .chat_id(ChatId::from(chat_id))
        .build();

    let description = api.get_chat(&get_chat_params).unwrap().result.description;

    if description.is_none() || &description.unwrap() != text {
        api.set_chat_description(&set_chat_description_params).unwrap();
    };
}
