Requirements:
1. Set environment variables:
   * "TOKEN" for Telegram token
   * "CHAT_ID" for Telegram chat which will get the messages

---
For deployment:
1. Set up secrets (replace "***" with relevant values)
```
flyctl secrets set TOKEN="***" CHAT_ID="***"
```
2. Build docker image and deploy
```
DOCKER_BUILDKIT=1 flyctl deploy --local-only
```

Thanks to fly.io team for hosting!